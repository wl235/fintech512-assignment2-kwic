import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

public class TitleAndKey {

    private ArrayList<Title> titleKey;

    public TitleAndKey(Input input) {
        ArrayList<String> Ignores = input.getIgnores();
        ArrayList<String> lines = input.getLines();
        ArrayList<ArrayList<String>> words = input.getWords();

        this.titleKey = new ArrayList<Title>();
        for (int i = 0; i < words.size(); i++) {
            for(int j=0; j < words.get(i).size();j++){
                if(!Ignores.contains(words.get(i).get(j).toLowerCase())){
                    String key = words.get(i).get(j).toUpperCase();
                    titleKey.add(new Title(mergeWords(words.get(i),j),key));

                }
            }
        }

    }

    public String mergeWords(ArrayList<String> Words, int n) {
        String line = "";
        for (int i=0;i< Words.size();i++) {
            if(i==n){
                line+=Words.get(i).toUpperCase();
                line+=" ";
            }
            else{
                line+=Words.get(i).toLowerCase();
                line+=" ";
            }
        }
        line = line.substring(0,line.length() - 1);
        return line;

    }
    public String returnLine(){
        String line = "";
        for(int i=0;i<this.titleKey.size();i++){
            //System.out.println(titleKey.get(i).line);
            line+=titleKey.get(i).title;
            line+="\n";
        }
        line = line.substring(0,line.length() - 1);
        return line;
    }
    public void printLine(){
        for(int i=0;i<this.titleKey.size();i++){
            System.out.println(titleKey.get(i).title);
        }
    }

    public void sortLine(){
        titleKey.sort(new Comparator<Title>() {
            @Override
            public int compare(Title o1, Title o2) {
                return o1.keyword.compareTo(o2.keyword);

            }
        });

    }


}
