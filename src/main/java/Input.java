import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

public class Input {

    private ArrayList<String> WordsToIgnore;
    private ArrayList<String> title;
    private ArrayList<ArrayList<String>> words;

    public void inputfile() {
        this.WordsToIgnore = new ArrayList<String>();
        this.title = new ArrayList<String>();
        this.words = new ArrayList<ArrayList<String>>();


        Scanner myReader = new Scanner(System.in);
        while (myReader.hasNextLine()) {
            String data = myReader.nextLine();
            if (data.equals("::")) {
                break;
            }
            WordsToIgnore.add(data);
        }
        while (myReader.hasNextLine()) {
            String data = myReader.nextLine();
            addLine(data);
        }


    }

    public ArrayList<String> getIgnores() {
        return WordsToIgnore;
    }

    public ArrayList<String> getLines() {
        return title;
    }

    public ArrayList<ArrayList<String>> getWords() {
        return words;
    }

    public void addLine(String line) {
        title.add(line);
        words.add(splitLine(line));
    }

    public ArrayList<String> splitLine(String line) {
        return new ArrayList<String>(Arrays.asList(line.split(" ")));
    }
}